﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Cards
{
    /// <summary>
    /// Interaction logic for ResultTable.xaml
    /// </summary>
    public partial class ResultTable : UserControl
    {
        GameType gameType;
        private void loadFromFile()
        {
            string filename = "1.bin";
            Stream stream = File.Open(filename, FileMode.Open);
            BinaryFormatter bFormatter = new BinaryFormatter();
            Results = (List<Result>)bFormatter.Deserialize(stream);
            stream.Close();
        }

        private void saveToFile()
        {
            string filename = "1.bin";

            Stream stream = File.Open(filename, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, Results);
            stream.Close();
        }

        public MainWindow parentWindow { get; set; }
        public List<Result> Results = new List<Result>();

        public ResultTable(GameType gameType)
        {
            this.gameType = gameType;
            InitializeComponent();
            //Result res1 = new Result { Name = "Roman", Seconds = 100 };
            //Result res2 = new Result { Name = "Victor", Seconds = 200 };
            //Result res3 = new Result { Name = "Yuri", Seconds = 300 };
            //Results.Add(res1);
            //Results.Add(res2);
            //Results.Add(res3);
            loadFromFile();
            Results.Sort((x, y) =>
    x.Seconds.CompareTo(y.Seconds));
            Results = Results.Take(5).ToList();
            this.MainGrid.ItemsSource = Results;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            parentWindow.showGameTable(gameType);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            Results.Clear();
            //this.MainGrid.ItemsSource = Results;
            this.MainGrid.Items.Refresh();
            saveToFile();
        }
    }
}
