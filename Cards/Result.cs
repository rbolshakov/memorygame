﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    [Serializable]
    public class Result
    {
        public string Name { get; set; }
        public int Seconds { get; set; }
        public string Score
        {
            get
            {
                int minutes = Seconds / 60;
                int seconds = Seconds % 60;
                return String.Format("{0} мин {1} сек", minutes, seconds);
            }
        }
    }
}
