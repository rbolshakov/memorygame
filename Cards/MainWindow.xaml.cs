﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Test.UserControls;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Cards
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            showWelcomeScreen();
        }

        public void showWelcomeScreen()
        {
            WelcomeScreen screen = new WelcomeScreen();
            screen.parentWindow = this;
            this.contentControl.Content = screen;
        }

        public void showResultsTable(GameType gameType)
        {
            ResultTable table = new ResultTable(gameType);
            table.parentWindow = this;
            this.contentControl.Content = table;
        }

        public void showGameTable(GameType gameType)
        {
            GameTable table = new GameTable(gameType);
            table.parentWindow = this;
            table.parentWindow.Title = "Подводный мир";
            this.contentControl.Content = table;
        }
    }
}
