﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace Cards
{
    public class CardCreator
    {
        static String[] wildAnimals = { "БЕЛЫЙ МЕДВЕДЬ", "БУРЫЙ МЕДВЕДЬ", "ВЕРБЛЮД", "ЖИРАФ", "ЗЕБРА", "КЕНГУРУ", "ЛЕВ", "НОСОРОГ", "ОБЕЗЬЯНА", "ОЛЕНЬ", "СЛОН", "ТИГР"};
        static String[] tameAnimals = { "ГУСЬ", "КОЗЛЕНОК", "КОРОВА", "КОШКА", "КРОЛИКИ", "ЛОШАДЬ", "ОВЦА", "ОСЛИК", "ПЕТУХ", "ПОРОСЕНОК", "СОБАКА", "ЦЫПЛЕНОК"};
        static String[] fish = {"Акула","Ёрш","Камбала","Карась","Карп","Окунь","Осётр","Сом","Стерлядь","Угорь","Форель","Щука" };
        static String[] birds = {"Аист","Голубь","Дятел","Кукушка","Ласточка","Лебедь", "Синица", "Скворец", "Снегирь", "Сова", "Сорока", "Чайка" };
        static String[] insects = { "Бабочка", "Божья коровка", "Комар", "Кузнечик", "Майский жук", "Муравей", "Муха", "Оса", "Паук", "Пчела", "Стрекоза", "Таракан" };
        public static BitmapImage getCardImage(int typeOfCard, GameType gameType)
        {
            String imageString = "";
            switch (gameType)
            {
                case GameType.Wild:
                    imageString = @"WildAnimals/" + wildAnimals[typeOfCard - 1] + ".JPG";
                    break;
                case GameType.Tame:
                    imageString = @"TameAnimals/" + tameAnimals[typeOfCard - 1] + ".JPG";
                    break;
                case GameType.Fish:
                    imageString = @"Fish/" + fish[typeOfCard - 1] + ".JPG";
                    break;
                case GameType.Birds:
                    imageString = @"Birds/" + birds[typeOfCard - 1] + ".JPG";
                    break;
                case GameType.Insects:
                    imageString = @"Insects/" + insects[typeOfCard - 1] + ".JPG";
                    break;

            }
            return new BitmapImage(new Uri(imageString, UriKind.Relative));
        }
    }
}
