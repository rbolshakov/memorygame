﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Test.UserControls;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Wpf3D;
using System.Media;

namespace Cards
{
    /// <summary>
    /// Interaction logic for GameTable.xaml
    /// </summary>
    /// 

    public enum GameType
    {
        Wild,
        Tame,
        Fish,
        Birds,
        Insects
    }
    public partial class GameTable : UserControl
    {
        SoundPlayer player;
        public MainWindow parentWindow { get; set; }

        public GameTable(GameType gameType)
        {
            InitializeComponent();
            this.gameType = gameType;
            GiveCards();
            score = 0;
        }

        Card openedCard;
        Card currentCard;
        String soundPath = @"Sounds\\waterfall.wav";

        bool animationInProgress;
        int minutes;
        int seconds;
        int score;
        bool busy = true;

        GameType gameType = GameType.Fish;

        DispatcherTimer dispatcherTimer;

        List<Card> cards = new List<Card>();

        private void startTimer()
        {
            resetTimer();
            
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0,0,1);
            dispatcherTimer.Start();
        }

        private void stopTimer()
        {
            if (dispatcherTimer != null)
                dispatcherTimer.Stop();
        }

        private void resetTimer()
        {
            minutes = 0;
            seconds = 0;
            timeLabel.Content = string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            seconds++;
            if (seconds >= 60)
            {
                minutes++;
                seconds = 0;
            }

            timeLabel.Content = string.Format("{0}:{1}", minutes.ToString("00"), seconds.ToString("00"));
        }

        private void GiveCards(int timeOffset = 0)
        {
            busy = true;
            cards = new List<Card>();
            Random random = new Random();
            List<int> types = new List<int> { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12 };
            InitializeComponent();
            for (int i = 2; i < 5; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    int num = random.Next(types.Count);
                    int typeOfCard = types[num];
                    BitmapImage backImage = CardCreator.getCardImage(typeOfCard, gameType);
                    BitmapImage frontImage = new BitmapImage(new Uri(@"Design/cardFront.jpg", UriKind.Relative));

                    Card card = new Card();
                    Image frontImage1 = new Image();
                    frontImage1.Source = frontImage;
                    card.Front = frontImage1;
                    Image backImage1 = new Image();
                    backImage1.Source = backImage;
                    card.Back = backImage1;

                    card.opened = false;

                    card.typeOfCard = typeOfCard;
                    types.RemoveAt(num);
                 
                    MainGrid.Children.Add(card);
                    Grid.SetRow(card, i);
                    Grid.SetColumn(card, j);
                    card.Margin = new Thickness(5);
                    card.MouseDown += new MouseButtonEventHandler(Image_MouseDown);
                    int ms = ((i - 1) * 8 + j) * 200 + timeOffset;
                    card.Translate(new TimeSpan(0, 0, 0, 0, ms), 0, 0, new EventHandler(GiveAnimation_Finished)); 
                }
            }
        }

        private void Remove_Cards()
        {
            busy = true;
            if (cards.Count == 0)
            {
                GiveCards();
            }
            foreach (Card card in cards)
            {
                int ms = ((Grid.GetRow(card) - 1) * 8 + Grid.GetColumn(card)) * 200;
                card.Translate(new TimeSpan(0, 0, 0, 0, ms), 1000, -1000, new EventHandler(RemoveAnimation_Finished));
            }
        }

        private void Animation_Remove_Finished(object sender, EventArgs args)
        {
            animationInProgress = false;
            MainGrid.Children.Remove(openedCard);
            MainGrid.Children.Remove(currentCard);
            cards.Remove(openedCard);
            cards.Remove(currentCard);
            score++;
            scoreLabel.Content = string.Format("{0} из 24", score);
            openedCard = null;

            if (score == 24)
            {
                stopTimer();
                ScaleResult();

                SaveScore scoreDialog = new SaveScore();
                bool? result = scoreDialog.ShowDialog();
                if (result == true)
                {
                    string userName = scoreDialog.UserName;

                    Result playerResult = new Result { Name = userName, Seconds = minutes * 60 + seconds };
                    List<Result> results = loadFromFile();
                    results.Add(playerResult);
                    saveToFile(results);
                    this.parentWindow.showResultsTable(gameType);
                }
                else
                {
                    ResetGame();
                }
            }
        }

        private void Reset_Score()
        {
            score = 0;
            scoreLabel.Content = string.Format("{0} из 24", score);
        }

        private void GiveAnimation_Finished(object sender, EventArgs args)
        {
            Card card = (Card)Storyboard.GetTarget(((sender as AnimationClock).Timeline as AnimationTimeline));
            cards.Add(card);
            if (cards.Count == 24)
            {
                Reset_Score();
                startTimer();
                busy = false;
            }
        }

        private List<Result> loadFromFile()
        {
            string filename = "1.bin";
            try
            {
                Stream stream = File.Open(filename, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                List<Result> results = (List<Result>)bFormatter.Deserialize(stream);
                stream.Close();
                return results;
            }
            catch
            {

            }
            finally
            {

            }
            return new List<Result>();
        }

        private void saveToFile(List<Result> results)
        {
            string filename = "1.bin";

            Stream stream = File.Open(filename, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, results);
            stream.Close();
        }

        private void RemoveAnimation_Finished(object sender, EventArgs args)
        {
            Card card = (Card)Storyboard.GetTarget(((sender as AnimationClock).Timeline as AnimationTimeline));
            cards.Remove(card);
            if (cards.Count == 0)
                GiveCards();

        }

        private void Animation_Finished(object sender, EventArgs args)
        {
            animationInProgress = false;
            if (openedCard == null)
            {
                openedCard = currentCard;
            }
            else if (openedCard.typeOfCard == currentCard.typeOfCard)
            {
                animationInProgress = true;
                openedCard.Scale(new EventHandler(Animation_Remove_Finished));
                currentCard.Scale();
            }
            else
            {
                openedCard.opened = false;
                openedCard.Flip();
                currentCard.opened = false;
                currentCard.Flip();
                openedCard = null;
            }
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Card card = (Card)sender;
            if (card.opened || animationInProgress || busy)
                return;

            animationInProgress = true;
            card.Flip(new EventHandler(Animation_Finished));
            card.opened = true;
            currentCard = card;
        }

        private void fc_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ResetGame()
        {
            HideResult();
            stopTimer();
            Remove_Cards();
        }

        private void Reset_Button_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            //this.parentWindow.showResultsTable();
            ResetGame();
        }

        /*
        private void Change_Button_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            //this.parentWindow.showResultsTable();
            cardWildPictures = !cardWildPictures;
            if (cardWildPictures)
            {
                this.parentWindow.Title = "В зоопарке";
            }
            else
            {
                this.parentWindow.Title = "На ферме";
            }
            ResetGame();
        }*/

        private void SelectGameHandler(object sender)
        {
            MenuItem item = sender as MenuItem;
            MenuItem parent = item.Parent as MenuItem;
            foreach (MenuItem child in parent.Items)
            {
                child.IsChecked = false;
            }
            item.IsChecked = true;
        }
        private void DomesticAnimal_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            SelectGameHandler(sender);
            gameType = GameType.Tame;
            this.parentWindow.Title = "На ферме";
            ResetGame();
        }
        private void WildAnimal_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            SelectGameHandler(sender);
            gameType = GameType.Wild;
            this.parentWindow.Title = "В зоопарке";
            ResetGame();
        }
        private void Fish_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            SelectGameHandler(sender);
            gameType = GameType.Fish;
            this.parentWindow.Title = "Подводный мир";
            ResetGame();
        }

        private void Birds_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            SelectGameHandler(sender);
            gameType = GameType.Birds;
            this.parentWindow.Title = "Птицы";
            ResetGame();
        }

        private void Insects_Click(object sender, RoutedEventArgs e)
        {
            if (busy)
                return;
            SelectGameHandler(sender);
            gameType = GameType.Insects;
            this.parentWindow.Title = "Насекомые";
            ResetGame();
        }

        private void StartSound_Click(object sender, RoutedEventArgs e)
        {
            if (player != null)
            {
                StopSound();
            }
            else
            {
                StartSound();
            }
        }

        private void StartSound()
        {
            player = new SoundPlayer(soundPath);
            player.Load();
            player.PlayLooping();
        }

        private void StopSound()
        {
            player.Stop();
            player = null;
        }

        private void FirstSound_Click(object sender, RoutedEventArgs e)
        {
            String oldPath = soundPath;
            soundPath = @"Sounds\\waterfall.wav";
            if (oldPath != soundPath) 
            { 
                SelectSound_Click(sender, e);
            }
        }

        private void SecondSound_Click(object sender, RoutedEventArgs e)
        {
            String oldPath = soundPath;
            soundPath = @"Sounds\\train.wav";
            if (oldPath != soundPath)
            {
                SelectSound_Click(sender, e);
            }
        }

        private void SelectSound_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            MenuItem parent = item.Parent as MenuItem;
            foreach (MenuItem child in parent.Items)
            {
                child.IsChecked = false;
            }
            item.IsChecked = true;

            if (player != null)
            {
                StopSound();
                StartSound();
            }
        }

        public void HideResult()
        {
            var animation = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 1)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            animation.To = 0.01;
            transform.BeginAnimation(ScaleTransform.ScaleXProperty, animation);

            var animation2 = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 1)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            animation.To = 0.01;
            transform.BeginAnimation(ScaleTransform.ScaleYProperty, animation);

            DoubleAnimation da = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 1)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            da.From = 1;
            da.To = 0;
            da.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 1));
            da.AutoReverse = false;
            resultLabel.BeginAnimation(OpacityProperty, da);
        }

        public void ScaleResult()
        {
            var animation = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            animation.To = 1.0;
            transform.BeginAnimation(ScaleTransform.ScaleXProperty, animation);

            var animation2 = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0,0,0,0,300)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            animation.To = 1.0;
            transform.BeginAnimation(ScaleTransform.ScaleYProperty, animation);

            DoubleAnimation da = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500));
            da.AutoReverse = false;
            resultLabel.BeginAnimation(OpacityProperty, da);
        }

        private void Exit_Button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
