﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace Cards
{
    /// <summary>
    /// Interaction logic for WelcomeScreen.xaml
    /// </summary>
    public partial class WelcomeScreen : UserControl
    {
        public MainWindow parentWindow { get; set; }
        public WelcomeScreen()
        {
            InitializeComponent();
            animateTitle();
            animateDescription();
            animateAuthor();
            animateName();
            animateRules();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //TitleLabel.Opacity = 0;
            this.parentWindow.showGameTable(GameType.Fish);
        }

        private void animateDescription()
        {
            var animation = new ThicknessAnimation();
            animation.From = new Thickness(0, 200, 0, 0);
            animation.To = new Thickness(0, 0, 0, 0);
            animation.Duration = TimeSpan.FromSeconds(0.3);
            animation.BeginTime = TimeSpan.FromMilliseconds(800);
            DescLabel.BeginAnimation(MarginProperty, animation);
            DescLabel2.BeginAnimation(MarginProperty, animation);

            DoubleAnimation da = new DoubleAnimation();
            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 1000));
            da.AutoReverse = false;
            da.BeginTime = TimeSpan.FromMilliseconds(800);
            DescLabel.BeginAnimation(OpacityProperty, da);
            DescLabel2.BeginAnimation(OpacityProperty, da);
        }

        private void animateTitle()
        {
            var animation = new DoubleAnimation()
            {
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300)),
                EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut }
            };
            animation.To = 1;
            animation.BeginTime = TimeSpan.FromMilliseconds(500);
            transform.BeginAnimation(ScaleTransform.ScaleXProperty, animation);
            transform.BeginAnimation(ScaleTransform.ScaleYProperty, animation);

            DoubleAnimation da = new DoubleAnimation();
            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 300));
            da.AutoReverse = false;
            da.BeginTime = TimeSpan.FromMilliseconds(500);
            TitleLabel.BeginAnimation(OpacityProperty, da);
        }

        private void animateAuthor()
        {
            var animation = new ThicknessAnimation();
            animation.From = new Thickness(1000, 0, 0, 0);
            animation.To = new Thickness(250, 0, 0, 0);
            animation.Duration = TimeSpan.FromSeconds(0.5);
            animation.BeginTime = TimeSpan.FromMilliseconds(1100);
            AuthorLabel.BeginAnimation(MarginProperty, animation);
        }

        private void animateName()
        {
            var animation = new ThicknessAnimation();
            animation.From = new Thickness(1000, 0, 0, 0);
            animation.To = new Thickness(250, 0, 0, 0);
            animation.Duration = TimeSpan.FromSeconds(0.5);
            animation.BeginTime = TimeSpan.FromMilliseconds(1500);
            NameLabel.BeginAnimation(MarginProperty, animation);
        }

        private void animateRules()
        {
            DoubleAnimation da = new DoubleAnimation();
            da.From = 0;
            da.To = 1;
            da.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 600));
            da.AutoReverse = false;
            da.BeginTime = TimeSpan.FromMilliseconds(2000);
            //RulesLabel.BeginAnimation(OpacityProperty, da);

            DoubleAnimation da2 = new DoubleAnimation();
            da2.From = 0;
            da2.To = 1;
            da2.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 600));
            da2.AutoReverse = false;
            da2.BeginTime = TimeSpan.FromMilliseconds(2000);
            //RulesBlock.BeginAnimation(OpacityProperty, da2);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Rules rulesWindow = new Rules();
            rulesWindow.Show();
        }
    }
}
