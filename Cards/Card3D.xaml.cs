﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;

namespace Wpf3D
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Card3D : UserControl
    {
        private IEasingFunction EasingFunction = new SineEase() { EasingMode = EasingMode.EaseInOut };
        private Duration FlipDuration = (Duration)TimeSpan.FromSeconds(0.5);

        public int gameType { get; set; }
        public bool opened { get; set; }

        public BitmapImage Back { get; set; }
        public BitmapImage Front { get; set; }

        public bool IsFlipped { get; set; }

        /*
        public static readonly DependencyProperty FrontProperty =
            DependencyProperty.Register("Front", typeof(UIElement), typeof(UserControl1), new UIPropertyMetadata(null));
        public UIElement Front
        {
            get { return (UIElement)GetValue(FrontProperty); }
            set { SetValue(FrontProperty, value); }
        }

        public static readonly DependencyProperty BackProperty =
            DependencyProperty.Register("Back", typeof(UIElement), typeof(UserControl1), new UIPropertyMetadata(null));
        public UIElement Back
        {
            get { return (UIElement)GetValue(BackProperty); }
            set { SetValue(BackProperty, value); }
        }*/

        public ModelVisual3D model;

        public GeometryModel3D front;
        public GeometryModel3D back;
        public Card3D()
        {
            InitializeComponent();
            InitModel();
        }

        public Card3D(BitmapImage front, BitmapImage back)
        {
            this.Front = front;
            this.Back = back;
            InitializeComponent();
            InitModel();
        }

        public void InitModel()
        {
            ClearViewport();


            front = createFront();
            back = createBack();

            AmbientLight light = new AmbientLight(Colors.White);
            DirectionalLight light2 = new DirectionalLight(Colors.White, new Vector3D(2, 0, -1));
            DirectionalLight light3 = new DirectionalLight(Colors.White, new Vector3D(2, 0, 0));
            DirectionalLight light4 = new DirectionalLight(Colors.White, new Vector3D(-2, 0, 0));


            Model3DGroup group = new Model3DGroup();
            group.Children.Add(front);
            group.Children.Add(back);
            group.Children.Add(light);
            group.Children.Add(light2);
            group.Children.Add(light3);
            group.Children.Add(light4);

            model = new ModelVisual3D();
            model.Content = group;
            this.mainViewport.Children.Add(model);
        }

        GeometryModel3D createFront()
        {
            MeshGeometry3D triangleMesh = new MeshGeometry3D();

            Point3D point0 = new Point3D(3.5, 5, 0);
            Point3D point1 = new Point3D(-3.5, 5, 0);
            Point3D point2 = new Point3D(3.5, -5, 0);
            Point3D point3 = new Point3D(-3.5, -5, 0);

            triangleMesh.Positions.Add(point0);
            triangleMesh.Positions.Add(point1);
            triangleMesh.Positions.Add(point2);
            triangleMesh.Positions.Add(point3);

            triangleMesh.TriangleIndices.Add(0);
            triangleMesh.TriangleIndices.Add(1);
            triangleMesh.TriangleIndices.Add(2);
            triangleMesh.TriangleIndices.Add(2);
            triangleMesh.TriangleIndices.Add(1);
            triangleMesh.TriangleIndices.Add(3);

            Vector3D normal = new Vector3D(0, 0, 1);
            triangleMesh.Normals.Add(normal);
            triangleMesh.Normals.Add(normal);
            triangleMesh.Normals.Add(normal);
            triangleMesh.Normals.Add(normal);

            Material material = new DiffuseMaterial(new SolidColorBrush(Colors.DarkKhaki));

            //var uriSource = new Uri(@"C:\Users\Roman\Documents\Visual Studio 2013\Projects\Wpf3D\Wpf3D\cat.jpg", UriKind.Absolute);
            //var bitmap = new BitmapImage(uriSource);
            TransformedBitmap myRotatedBitmapSource = new TransformedBitmap();
            myRotatedBitmapSource.BeginInit();
            myRotatedBitmapSource.Source = this.Front;
            myRotatedBitmapSource.Transform = new RotateTransform(-90);
            myRotatedBitmapSource.EndInit();

            var brush = new ImageBrush(myRotatedBitmapSource);
            //var grp = new TransformGroup();
            //grp.Children.Add(new ScaleTransform(0.25, 0.65, 0.5, 0.5));
            //grp.Children.Add(new TranslateTransform(0.0, 0.0));
            //brush.Transform = grp;
            material = new DiffuseMaterial(brush);

            triangleMesh.TextureCoordinates.Add(new Point(0, 0));
            triangleMesh.TextureCoordinates.Add(new Point(0, 1));
            triangleMesh.TextureCoordinates.Add(new Point(1, 0));
            triangleMesh.TextureCoordinates.Add(new Point(1, 1));

            GeometryModel3D triangleModel = new GeometryModel3D(triangleMesh, material);
            return triangleModel;
        }

        GeometryModel3D createBack()
        {
            MeshGeometry3D triangleMesh = new MeshGeometry3D();

            Point3D point0 = new Point3D(3.5, 5, 0);
            Point3D point1 = new Point3D(-3.5, 5, 0);
            Point3D point2 = new Point3D(3.5, -5, 0);
            Point3D point3 = new Point3D(-3.5, -5, 0);

            triangleMesh.Positions.Add(point0);
            triangleMesh.Positions.Add(point1);
            triangleMesh.Positions.Add(point2);
            triangleMesh.Positions.Add(point3);

            triangleMesh.TriangleIndices.Add(0);
            triangleMesh.TriangleIndices.Add(2);
            triangleMesh.TriangleIndices.Add(1);
            triangleMesh.TriangleIndices.Add(1);
            triangleMesh.TriangleIndices.Add(2);
            triangleMesh.TriangleIndices.Add(3);

            Vector3D normal = new Vector3D(0, 0, -1);
            triangleMesh.Normals.Add(normal);
            triangleMesh.Normals.Add(normal);
            triangleMesh.Normals.Add(normal);
            triangleMesh.Normals.Add(normal);

            Material material = new DiffuseMaterial(new SolidColorBrush(Colors.DarkKhaki));

            //var uriSource = new Uri(@"C:\Users\Roman\Documents\Visual Studio 2013\Projects\Wpf3D\Wpf3D\dog.jpg", UriKind.Absolute);
            //var bitmap = new BitmapImage(uriSource);

            TransformedBitmap myRotatedBitmapSource = new TransformedBitmap();
            myRotatedBitmapSource.BeginInit();
            myRotatedBitmapSource.Source = this.Back;
            TransformGroup group = new TransformGroup();
            group.Children.Add(new ScaleTransform(-1, 1));
            group.Children.Add(new RotateTransform(-90));
            myRotatedBitmapSource.Transform = group;
            myRotatedBitmapSource.EndInit();

            var brush = new ImageBrush(myRotatedBitmapSource);
            //var grp = new TransformGroup();
            //grp.Children.Add(new ScaleTransform(0.25, 0.65, 0.5, 0.5));
            //grp.Children.Add(new TranslateTransform(0.0, 0.0));
            //brush.Transform = grp;
            material = new DiffuseMaterial(brush);

            triangleMesh.TextureCoordinates.Add(new Point(0, 0));
            triangleMesh.TextureCoordinates.Add(new Point(0, 1));
            triangleMesh.TextureCoordinates.Add(new Point(1, 0));
            triangleMesh.TextureCoordinates.Add(new Point(1, 1));

            GeometryModel3D triangleModel = new GeometryModel3D(triangleMesh, material);
            return triangleModel;
        }

        private void simpleButton_Click(object sender, RoutedEventArgs e)
        {

            /*
            AxisAngleRotation3D ay3d = new AxisAngleRotation3D(new Vector3D(0, 1, 0), 1);
            RotateTransform3D myRotateTransform2 = new RotateTransform3D(ay3d, new Point3D(0, 0, 0));
            front.Transform = myRotateTransform2;
            back.Transform = myRotateTransform2;

            DoubleAnimation angleAnimation = new DoubleAnimation();
            angleAnimation.From = 0.0d;
            angleAnimation.To = 180;
            angleAnimation.Duration = new Duration(new TimeSpan(0, 0, 2));

            QuarticEase easingFunction = new QuarticEase();
            easingFunction.EasingMode = EasingMode.EaseOut;

            angleAnimation.EasingFunction = easingFunction;
            angleAnimation.AutoReverse = true;

            ay3d.BeginAnimation(AxisAngleRotation3D.AngleProperty, angleAnimation);*/


        }

        private Model3DGroup CreateTriangleModel(Point3D p0, Point3D p1, Point3D p2)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();
            mesh.Positions.Add(p0);
            mesh.Positions.Add(p1);
            mesh.Positions.Add(p2);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);
            Vector3D normal = CalculateNormal(p0, p1, p2);
            mesh.Normals.Add(normal);
            mesh.Normals.Add(normal);
            mesh.Normals.Add(normal);
            Material material = new DiffuseMaterial(new SolidColorBrush(Colors.DarkKhaki));

            var uriSource = new Uri(@"C:\Users\Roman\Documents\Visual Studio 2013\Projects\Wpf3D\Wpf3D\sun.jpg", UriKind.Absolute);
            var bitmap = new BitmapImage(uriSource);

            var brush = new ImageBrush(bitmap);
            //var grp = new TransformGroup();
            //grp.Children.Add(new ScaleTransform(0.25, 0.65, 0.5, 0.5));
            //grp.Children.Add(new TranslateTransform(0.0, 0.0));
            //brush.Transform = grp;
            material = new DiffuseMaterial(brush);

            mesh.TextureCoordinates.Add(new Point(0, 0));
            mesh.TextureCoordinates.Add(new Point(1, 0));
            mesh.TextureCoordinates.Add(new Point(0, 1));
            mesh.TextureCoordinates.Add(new Point(1, 1));

            GeometryModel3D model = new GeometryModel3D(mesh, material);




            Model3DGroup group = new Model3DGroup();
            group.Children.Add(model);
            return group;
        }

        private Model3DGroup CreateRectangleModel(Point3D p0, Point3D p1, Point3D p2, Point3D p3)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();
            mesh.Positions.Add(p0);
            mesh.Positions.Add(p1);
            mesh.Positions.Add(p2);
            //mesh.Positions.Add(p3);
            mesh.TriangleIndices.Add(0);
            mesh.TriangleIndices.Add(1);
            mesh.TriangleIndices.Add(2);
            // mesh.TriangleIndices.Add(1);
            // mesh.TriangleIndices.Add(2);
            // mesh.TriangleIndices.Add(3);

            Vector3D normal = CalculateNormal(p0, p1, p2);
            mesh.Normals.Add(normal);
            mesh.Normals.Add(normal);
            mesh.Normals.Add(normal);
            //  mesh.Normals.Add(normal);
            Material material = new DiffuseMaterial(new SolidColorBrush(Colors.DarkKhaki));

            var uriSource = new Uri(@"C:\Users\Roman\Documents\Visual Studio 2013\Projects\Wpf3D\Wpf3D\cat.jpg", UriKind.Absolute);
            var bitmap = new BitmapImage(uriSource);

            TransformedBitmap myRotatedBitmapSource = new TransformedBitmap();
            myRotatedBitmapSource.BeginInit();
            myRotatedBitmapSource.Source = bitmap;
            myRotatedBitmapSource.Transform = new RotateTransform(130);
            myRotatedBitmapSource.EndInit();

            var brush = new ImageBrush(myRotatedBitmapSource);
            //var grp = new TransformGroup();
            //grp.Children.Add(new ScaleTransform(0.25, 0.65, 0.5, 0.5));
            //grp.Children.Add(new TranslateTransform(0.0, 0.0));
            //brush.Transform = grp;
            //material = new DiffuseMaterial(brush);

            //mesh.TextureCoordinates.Add(new Point(0, 0));
            //mesh.TextureCoordinates.Add(new Point(0, 1));
            //mesh.TextureCoordinates.Add(new Point(1, 1));
            // mesh.TextureCoordinates.Add(new Point(1, 0));

            GeometryModel3D model = new GeometryModel3D(mesh, material);




            Model3DGroup group = new Model3DGroup();
            group.Children.Add(model);
            return group;
        }

        private Vector3D CalculateNormal(Point3D p0, Point3D p1, Point3D p2)
        {
            Vector3D v0 = new Vector3D(p1.X - p0.X, p1.Y - p0.Y, p1.Z - p0.Z);
            Vector3D v1 = new Vector3D(p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
            return Vector3D.CrossProduct(v0, v1);
        }

     
        
        private void ClearViewport()
        {
            
            ModelVisual3D m;
            for (int i = mainViewport.Children.Count - 1; i >= 0; i--)
            {
                m = (ModelVisual3D)mainViewport.Children[i];
                if (m.Content is DirectionalLight == false)
                    mainViewport.Children.Remove(m);
            }
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            simpleButton_Click(null, null);
        }

        public void Flip()
        {
            AxisAngleRotation3D ay3d = new AxisAngleRotation3D(new Vector3D(0, 1, 0), 1);
            RotateTransform3D myRotateTransform2 = new RotateTransform3D(ay3d, new Point3D(0, 0, 0));
            front.Transform = myRotateTransform2;
            back.Transform = myRotateTransform2;

            DoubleAnimation angleAnimation = new DoubleAnimation();
            if (!this.IsFlipped)
            {
                angleAnimation.From = 0.0d;
                angleAnimation.To = 180;
            }
            else
            {
                angleAnimation.From = 180.0d;
                angleAnimation.To = 0;
            }
            angleAnimation.Duration = new Duration(new TimeSpan(0, 0, 1));

            QuarticEase easingFunction = new QuarticEase();
            easingFunction.EasingMode = EasingMode.EaseOut;

            angleAnimation.EasingFunction = easingFunction;
            angleAnimation.AutoReverse = false;

            ay3d.BeginAnimation(AxisAngleRotation3D.AngleProperty, angleAnimation);
            IsFlipped = !IsFlipped;
            //OnFlipped(new EventArgs());
        }

        public void Flip(EventHandler handler)
        {
            AxisAngleRotation3D ay3d = new AxisAngleRotation3D(new Vector3D(0, 1, 0), 1);
            RotateTransform3D myRotateTransform2 = new RotateTransform3D(ay3d, new Point3D(0, 0, 0));
            front.Transform = myRotateTransform2;
            back.Transform = myRotateTransform2;

            DoubleAnimation angleAnimation = new DoubleAnimation();
            if (!this.IsFlipped)
            {
                angleAnimation.From = 0.0d;
                angleAnimation.To = 180;
            }
            else
            {
                angleAnimation.From = 180.0d;
                angleAnimation.To = 0;
            }
            angleAnimation.Duration = new Duration(new TimeSpan(0, 0, 1));

            QuarticEase easingFunction = new QuarticEase();
            easingFunction.EasingMode = EasingMode.EaseOut;

            angleAnimation.EasingFunction = easingFunction;
            angleAnimation.AutoReverse = false;

            angleAnimation.Completed += handler;

            ay3d.BeginAnimation(AxisAngleRotation3D.AngleProperty, angleAnimation);
            IsFlipped = !IsFlipped;
     
        }

        
        public void Scale(EventHandler handler)
        {
            var animation = new DoubleAnimation()
            {
                Duration = FlipDuration,
                EasingFunction = EasingFunction,
            };
            animation.Completed += handler;
            animation.To = 0.5;
            scaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, animation);

            var animation2 = new DoubleAnimation()
            {
                Duration = FlipDuration,
                EasingFunction = EasingFunction,
            };
            animation.To = 0.5;
            scaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, animation);

            DoubleAnimation da = new DoubleAnimation();
            da.From = 1;
            da.To = 0;
            da.Duration = FlipDuration;
            da.AutoReverse = false;
            this.BeginAnimation(OpacityProperty, da);
        }

        public void Scale()
        {
            var animation = new DoubleAnimation()
            {
                Duration = FlipDuration,
                EasingFunction = EasingFunction,
            };
            animation.To = 0.5;
            scaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, animation);

            var animation2 = new DoubleAnimation()
            {
                Duration = FlipDuration,
                EasingFunction = EasingFunction,
            };
            animation.To = 0.5;
            scaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, animation);

            DoubleAnimation da = new DoubleAnimation();
            da.From = 1;
            da.To = 0;
            da.Duration = FlipDuration;
            da.AutoReverse = false;
            this.BeginAnimation(OpacityProperty, da);
        }

        public void Translate(TimeSpan time, int x, int y, EventHandler handler)
        {
            var animation = new DoubleAnimation()
            {
                Duration = FlipDuration,
                EasingFunction = EasingFunction,
            };
            animation.BeginTime = time;
            animation.To = y;
            transTransform.BeginAnimation(TranslateTransform.YProperty, animation);

            var animation2 = new DoubleAnimation()
            {
                Duration = FlipDuration,
                EasingFunction = EasingFunction,
            };
            animation2.BeginTime = time;
            animation2.To = x;
            Storyboard.SetTarget(animation2, this);
            animation2.Completed += handler;
            transTransform.BeginAnimation(TranslateTransform.XProperty, animation2);
        }
        
    }
}
